angular
	.module('minorThings')
		.factory('appService', ['$http', function($http) {
			return {
				getProducts: getProducts,
				getComments: getComments,
				saveComment: saveComment
			};

			function getProducts() {
				return $http.get('https://prod-54bf2.firebaseio.com/data.json');
			}

			function getComments() {
				return $http.get('https://prod-54bf2.firebaseio.com/comments.json');
			}

			function saveComment(comment) {
				return $http.post('https://prod-54bf2.firebaseio.com/comments.json', comment);
			}

		}]);

