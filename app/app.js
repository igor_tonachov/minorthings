(function(){

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyC0ZyqyBxwoZ_i_iMpUyHG-h4XAMxjU3wQ",
    authDomain: "prod-54bf2.firebaseapp.com",
    databaseURL: "https://prod-54bf2.firebaseio.com",
    storageBucket: "prod-54bf2.appspot.com",
    messagingSenderId: "202893002504"
  };
  firebase.initializeApp(config);


angular
	.module('minorThings', ['products', 'firebase'])
		.controller('ThisCtrl', [ '$scope', 'appService', function($scope, appService) {
			$scope.formIsOpen = false;
			$scope.isLogin = true;
			$scope.isRegistrating = false;
			$scope.posts =[];
			var auth = firebase.auth();

			$scope.toggleForm = toggleForm;
			$scope.switchForm = switchForm;
			$scope.saveComment = saveComment;
			$scope.registrateUser = registrateUser;
			$scope.loginUser = loginUser;
			$scope.signOut = signOut;


			function toggleForm() {
				$scope.formIsOpen = !$scope.formIsOpen;
			}

			function switchForm(){
				$scope.isLogin = !$scope.isLogin;
				$scope.isRegistrating = !$scope.isRegistrating;
			}

			function getProducts() {
				appService.getProducts().then(function(response) {
					$scope.products = response.data;
					$scope.currentProd = $scope.products[0];
				});
			}

			function getComments() {
				appService.getComments().then(function(response) {
					$scope.comments = response.data;
				});
			}

			function saveComment(comment, rating, product_id, user) {			
				appService.saveComment({comment, rating, product_id, user}).then(function(response) {
				  $scope.myComment = false;
					getComments();
				});
			}

			function loginUser(email, password) {
    		$scope.promise = auth.signInWithEmailAndPassword(email, password);
				$scope.promise.catch(e => console.log(e.message));
				$scope.formIsOpen = false;
			}

			function registrateUser(email, password) {
   			auth.createUserWithEmailAndPassword(email, password);
   			$scope.formIsOpen = false;
			}

			function signOut() {
				auth.signOut();
			}

    auth.onAuthStateChanged(firebaseUser => {
    	if (firebaseUser) {
    		$scope.currentUser = firebaseUser.providerData[0].uid;
    	} else {
    		console.log('user no exist');
    	}
    });


			getProducts();
			getComments();

		}]);

}());