angular
	.module('products', [])
		.controller('ProductsController', [ '$scope', 'appService', function($scope, appService){

			$scope.myComment = false;
			$scope.allComments = false;
			

			$scope.setCurrentProduct = setCurrentProduct;
			$scope.isCurrentProduct = isCurrentProduct;
			$scope.openCommentField = openCommentField;
			$scope.seeAllComments = seeAllComments;

				// rating

			$scope.userRating = 2;
	    $scope.rating2 = 5;
	    $scope.isReadonly = true;
	    $scope.rateFunction = function(rating) {
	    };


			function openCommentField() {
				$scope.allComments= !$scope.allComments; 
				$scope.myComment= false;
			}

			function seeAllComments() {
				$scope.myComment= !$scope.myComment; 
				$scope.allComments= false;
			}

			function setCurrentProduct(prod) {
				$scope.currentProd = prod;
				$scope.rating2 = setRating();
			}

			function setRating() {
				var count = [];
				var ratings = 0;
				angular.forEach($scope.comments, function(comment) {
					if(comment.product_id == $scope.currentProd.id) {
						if (comment.rating !== undefined) {
							ratings += comment.rating;
							count++;
						}
					}
				});
				return (ratings/count).toFixed(1)
			}

			function isCurrentProduct(prod) {
				return prod.image === $scope.currentProd.image
			}
			
		}]);